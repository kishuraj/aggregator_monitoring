package com.actuator1.Actuator1.service2;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

@Component
public class Beta_Aggregator implements HealthIndicator {
    SlackUtils slk = new SlackUtils();
    SlackUtilsUpMsg slk2 = new SlackUtilsUpMsg();

    @Scheduled(cron = "0 0/7 * * * ?")

    @Override
    public Health health() {
        try {
            URL siteurl = new URL("http://45.79.147.108:8080/");  //http://45.79.147.108:8080/   https://www.google.com/
            HttpURLConnection connection = (HttpURLConnection) siteurl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            /*.........*/
            checkagainforupmassage();
            int code = connection.getResponseCode();
            if (code == 200)
                return Health.up().build();
            else
                return Health.down().withDetail("error", "Oops! service is down").build();
        } catch (Exception e) {
            slk.msg();
            return Health.down().withDetail("error", "Beta down").build();

        }

    }

    public void checkagainforupmassage() {
        String lastmsg = SlackUtils.mylastmessage;
        if (lastmsg.contains("Down")) {
            try {
                URL siteurl = new URL("http://45.79.147.108:8080/");  //http://45.79.147.108:8080/   https://www.google.com/
                HttpURLConnection connection = (HttpURLConnection) siteurl.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                slk2.msg();
                SlackUtils.mylastmessage = "";
            } catch (Exception e) {
            }
        }
    }
}