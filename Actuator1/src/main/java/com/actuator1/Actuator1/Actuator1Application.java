package com.actuator1.Actuator1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

//enable scheduling
//@EnableScheduling
//created by Kishu Raj on 20-10-2020
@SpringBootApplication
public class Actuator1Application {

	public static void main(String[] args) {
		SpringApplication.run(Actuator1Application.class, args);

	}

}
