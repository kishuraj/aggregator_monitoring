package com.actuator1.Actuator1;

import com.actuator1.Actuator1.service.Prod_Aggreg;
import com.actuator1.Actuator1.service2.Beta_Aggregator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//enable scheduling
@EnableScheduling
@SpringBootApplication
public class Actuator1Application {

	public static void main(String[] args) {
		SpringApplication.run(Actuator1Application.class, args);

	}


}
